cfengine-augeas
===============

The cfengine-augeas tool is where promise theory meets bidirectional transformation.

An agent in CFEngine, using promise theory parlance, has a certain *degree of freedom*. A machine will have a *configuration space* based on the number and structure (and interpretation) of its configuration parameters. Each configurable component comprising a machine agent is *constrained* through a configuration policy: its degree of freedom being lowered as a result. CFEngine's approach (i.e. promises, patterns, convergence operations), is a very interesting one. I think there is one problem, however. We don't really know what the body of a promise actually *is*, though we often talk about the *type* of a promise, and of *bundles*, classes/*contexts* and more.

Promises in CFEngine currently wrap around plain old shell commands. Mind you, shell is Turing complete! So is sed, one of the many tools invoked from within scripts residing within the body of promises. No, the promise body should be written in the simplest of all languages.

What does that mean? Why should we care?

First, we have a hard time *comprehending* promises let alone *formally verifying* fairly complex policies. This is one reason why configuration management tools are way behind their programming language counterparts. No model checking. No type checking. Little analyses. Little syntheses. Rare optimization. Rare simplification. The list goes on.

Second, albeit theoretical, we are inviting the halting problem to the world of configuration management. Whatever we wrap around in a promise body should be written in a (decidable) weak language that can be parsed easily, while at the same time being 'atomic' enough to reason about its semantics. The syntax should be simple get/put, and nothing more.

The solution?

Augeas' bidirectional transformation approach does a good job of providing an abstrat view (in the form of a tree) that can be used to get/put values to some concrete configuration file, whose format is represented as a 'lens'. There are many lenses available for common configuration file formats. And writing new ones is not difficult, done through a subset of teh functional language ML. Since this is bidirectional, edits made directly on strings would be reflected on the tree (and vice versa). Cool.

^ we have a represenation of an *agent's degree of freedom* - and its configuration space, as an XPath tree. We can add cross-tree constraints, and possibly build some ontology on top of Augeas to associate meaning to configuration states. But that's another matter.

So, why don't we map the primitives of Augeas into CFEngine?

Both are written in C, with few dependencies.

Both have pretty solid theoretical foundation to back them up. They get the principles right.

Both are fairly generic. (Maybe even beyond CM)

Both aim to be atomic in some way. Only they aren't, in practice - specially CFEngine (the Puppet guys have a native provider).

----------

The cfengine-augeas prototype is written using Augeas' Python wrappers (just a proof of concept).  This GitHub project is where I plan to do the rewrite and proper development (most likely in C).
The *grand idea* is to automatically build micropromises and promise bundles from an arbitrary system's (current, possible) configuration state represented in an Augeas tree. One can then combine those promise primitives for complex policy specification. Next will be model checking, SATisfiability and whatnot ;)
